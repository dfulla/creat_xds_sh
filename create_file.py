


def create_file():

    file_xds = open('xds.sh', 'w+')
    file_xds.write('#SBATCH --partition=maxwell    # this is the default partition.\n')
    file_xds.write('#SBATCH --time=00:10:00        # default is 1h. The maximum is partition dependent, have a look at sview or scontrol for details.\n')
    file_xds.write('#SBATCH --nodes=1              # Number of nodes. If your job can consume variable number of nodes you might want to use something like\n')
    file_xds.write('#SBATCH --nodes=2-6            # which requests a minimum of 2 and a maximum of 6 nodes.\n')
    file_xds.close()


if __name__ == '__name__':

    create_file()



print('done')
